
<?php
$activeQuery = "1a";
include_once "obj/header.php";


include_once "obj/connect.php";

$name = "Stevens";
if (isset($_POST['doctorName']))
    $name = $_POST['doctorName'];

$sql = "SELECT Person.FirstName, Person.LastName, Patient.PhoneNumber FROM Person, Patient WHERE Person.PersonId = Patient.PersonID AND Person.PersonId IN( SELECT Patient.PersonID FROM Patient WHERE Patient.PatientID IN ( SELECT DISTINCT PatientVisit.PatientID FROM PatientVisit WHERE PatientVisit.DoctorID = ( SELECT Doctor.DoctorID FROM Doctor WHERE Doctor.PersonID = ( SELECT Person.PersonId FROM Person WHERE Person.LastName = \"$name\") ) ) ) ";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
?>

<section>
    <div class="alert alert-primary text-center" role="alert">
        <?php echo $sql; ?>
    </div>
    <div class="container">
        <p class="text-center" style="width: 50%; margin: 0 auto">
            This query is about getting all of Dr. Steven's Patients and their information so that we may
            contact them. You can re-run this page using the select below to change and test it.
            <br><br>
        </p>
    </div>
    <div class="container text-center" style="justify-content: center">
        <?php
        print "<pre>";
        print "<table border=1 style='margin: 0 auto'>";

        if (sizeof($result) > 0) {
            print "<tr>";
            foreach (array_keys($result[0]) as $key)
                print "<td>$key</td>";
            print "</tr>";
            foreach ($result as $r) {
                print "<tr>";
                foreach ($r as $value) {
                    print "<td>$value</td>";
                }
                print "</tr>";

            }
        }else
            print "No data found with that query. (No results found)";
        print "</table>";
        print "</pre>";
        echo '<br><br><br><br>';
        ?>
    </div>

    <div class="text-center">
        <h3>If you would like to try with another Doctor, please select from list below!</h3>
        <form action="" method="post" name="doctorName">
            <select name="doctorName">
                <?php
                $sql = "SELECT Person.LastName FROM Person WHERE Person.PersonId IN (SELECT Doctor.PersonID FROM Doctor)";
                $stmt= $link->prepare($sql);
                $stmt->execute();
                $result = $stmt->get_result();
                $result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows

                foreach ($result as $r){
                    echo '<option value="'.$r['LastName'].'" '.($name == $r['LastName'] ? ' selected="selected"' : "").'>'.$r['LastName'].'</option>';
                }

                ?>
            </select>
            <button type="submit" class="btn btn-primary">Change Doctor</button>
        </form>
    </div>
</section>

