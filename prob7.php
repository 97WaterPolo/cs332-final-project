
<?php
$activeQuery = "1a";
include_once "obj/header.php";


include_once "obj/connect.php";

if (isset($_POST['snapshot'])){
    $sqlQuery = "DROP TABLE IF EXISTS BK_Audit; CREATE TABLE BK_Audit AS SELECT * FROM Audit; DROP TABLE IF EXISTS BK_Doctor; CREATE TABLE BK_Doctor AS SELECT * FROM Doctor; DROP TABLE IF EXISTS BK_DoctorSpeciality; CREATE TABLE BK_DoctorSpeciality AS SELECT * FROM DoctorSpeciality; DROP TABLE IF EXISTS BK_PVisitPrescription; CREATE TABLE BK_PVisitPrescription AS SELECT * FROM PVisitPrescription; DROP TABLE IF EXISTS BK_PVisitTest; CREATE TABLE BK_PVisitTest AS SELECT * FROM PVisitTest; DROP TABLE IF EXISTS BK_Patient; CREATE TABLE BK_Patient AS SELECT * FROM Patient; DROP TABLE IF EXISTS BK_PatientVisit; CREATE TABLE BK_PatientVisit AS SELECT * FROM PatientVisit; DROP TABLE IF EXISTS BK_Person; CREATE TABLE BK_Person AS SELECT * FROM Person; DROP TABLE IF EXISTS BK_Prescription; CREATE TABLE BK_Prescription AS SELECT * FROM Prescription; DROP TABLE IF EXISTS BK_Speciality; CREATE TABLE BK_Speciality AS SELECT * FROM Speciality; DROP TABLE IF EXISTS BK_Test; CREATE TABLE BK_Test AS SELECT * FROM Test; ";
    foreach (explode(";", trim($sqlQuery)) as $sql){
        if (empty(trim($sql)) || trim($sql) == "" || trim($sql) == " ")
            continue;
        $stmt= $link->prepare($sql);
        if ($stmt->execute()){
            $changeErrorMsg = ("It was successfully updated<br>");
        }else
            $changeErrorMsg = ("Error creating the backup!");
    }
}

$sql = "SELECT TABLE_NAME, CREATE_TIME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA='DocOffice' AND TABLE_NAME LIKE 'BK_%'";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
?>
<br><br><br><br>
<div class="alert-danger text-center" style="padding: 5px; font-size: 18px;">
    The implementation below is <b>wrong!</b> My original understanding of this was that we had to use SQL statements, so I thought I would
    have to use the
    <br><i style="margin-left: 40px; background: lightgray; border: solid 1px black; font-size: 20px; color: black">BACKUP DATABASE DocOffice TO DISK = "C:\Users\Xander\CS332Final\DocOffice.sql"</i><br>
    <a href="https://www.w3schools.com/sql/sql_backup_db.asp" target="_blank">Documentation found here.</a>
    <br><br>
    Upon further discussing with the professor and finding out we can use the sql command line, I am now changing my answer to use the mysqldump command.
    <a href="https://mariadb.com/kb/en/mysqldump/" target="_blank">The documentation can be found here </a>and to use this you would run it as a bash/shell script on the
    server where your SQL database is hosted. What this will do is extract that database and save it into a .sql file. To ensure that you always have the latest value, you use the exact
    same name for the file so that when it runs it overwrites the old backup and writes in the new one. In my instance I would run:
    <br><i style="margin-left: 40px; background: lightgray; border: solid 1px black; font-size: 20px; color: black">mysqldump DocOffice > DocOffice.sql </i><br>
    and if I wanted to restore my SQL database from the file I would run
    <br><i style="margin-left: 40px; background: lightgray; border: solid 1px black; font-size: 20px; color: black">mysql DocOffice < DocOffice.sql</i><br>
</div>

<br><br><br><br>
<strike>
    <div>
        <div class="alert alert-primary text-center" role="alert">
            <?php echo $sql; ?>
        </div>
        <div class="container">
            <p class="text-center" style="width: 50%; margin: 0 auto">
                This returns all the backup databases. If you want to create a new backup click the button
                <br><br>
            </p>
        </div>
        <div class="text-center">

            <form method="post" action="">
                <button class="btn btn-success" type="submit" name="snapshot">Create Backup</button>
            </form>
        </div>
        <div class="container text-center" style="justify-content: center">
            <?php



            foreach ($result as $value){
                $sql = "SELECT * FROM ".$value['TABLE_NAME'];
                $stmt= $link->prepare($sql);
                $stmt->execute();
                $tableValues = $stmt->get_result();
                $tableValues = $tableValues->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
                print "<pre>";
                print "<h5>".$value['TABLE_NAME'].' created on '.$value['CREATE_TIME']."</h5>";
                print "<table border=1 style='margin: 0 auto'>";
                if (sizeof($tableValues) > 0) {
                    print "<tr>";
                    foreach (array_keys($tableValues[0]) as $key)
                        print "<td>$key</td>";
                    print "</tr>";
                    foreach ($tableValues as $r) {
                        print "<tr>";
                        foreach ($r as $value) {
                            print "<td>$value</td>";
                        }
                        print "</tr>";

                    }
                }else
                    print "No data found with that query. (No results found)";
                print "</table>";
                print "</pre>";
                echo '<br>';
            }



            ?>
        </div>
    </div>
</strike>

