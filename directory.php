
<?php
$activeQuery = "1a";
include_once "obj/header.php";


include_once "obj/connect.php";

$name = "Stevens";
if (isset($_POST['doctorName']))
    $name = $_POST['doctorName'];

$sql = "SELECT * FROM Patient";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
?>

<section>

    <div class="alert alert-primary text-center" role="alert">
        Please select a problem to view!
    </div>
    <div class="container">
        <div class="row">
            <a class="dropdown-item" href="prob1.php">1) Show all tables in DB</a>
        </div>
        <div class="row">
            <a class="dropdown-item" href="prob2.php">2) Dr. Robert's patients</a>
        </div>
        <div class="row">
            <a class="dropdown-item" href="prob3.php">3) Doctors with VICODIN</a>
        </div>
        <div class="row">
            <a class="dropdown-item" href="prob4.php">4) Doctor's Specialty View</a>
        </div>
        <div class="row">
            <a class="dropdown-item" href="prob5.php">5) Show all Doctor's Specialty</a>
        </div>
        <div class="row">
            <a class="dropdown-item" href="prob6.php">6) Trigger Creation Tester</a>
        </div>
        <div class="row">
            <a class="dropdown-item" href="prob7.php">7) Snapshot Of Backup</a>
        </div>
        <div class="row">
        </div>
    </div>
</section>

