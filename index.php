<?php
$activeQuery = 0;
include_once "obj/header.php";

?>
<style>
    .imgHover{
        transition: transform 0.25s ease;
    }

    .imgHover:hover{
        transform: scale(1.5);
    }
</style>

<header class="bg-primary text-white">
    <div class="container text-center">
        <h1>Welcome to Alexander Sigler's CPSC 323 Final Project</h1>
        <p class="lead">A simple website to test our my project and customization!</p>
        <a href="https://bitbucket.org/97WaterPolo/cs332-final-project/src/master/" target="_blank" style="color:limegreen; font-size: 18px">Click me to view the source code of this project on BitBucket</a>
    </div>
</header>

<section>

    <div class="text-center">
        <h1>Thank you so much professor! <3</h1>
        <span>Really appreciate the semester, I learned a lot and enjoyed the class!</span>
    </div>
    <br><br><br><br><br><br>
    <h2 class="text-center" style="color:green">From my previous assignment (too funny not to include)</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <img class="imgHover" src="imgs/big.png" style="width:100%;height:100%;">
            </div>
            <div class="col-lg-3">
                <img class="imgHover" src="imgs/hit.jpg" style="width:100%;height:100%;">
            </div>
            <div class="col-lg-3">
                <img class="imgHover" src="imgs/video.jpg" style="width:100%;height:100%;">
            </div>
            <div class="col-lg-3">
                <img class="imgHover" src="imgs/database.jpg" style="width:100%;height:100%;">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 text-center">
                <h1>Big</h1>
            </div>
            <div class="col-lg-3 text-center">
                <h1>Hit</h1>
            </div>
            <div class="col-lg-3 text-center">
                <h1>Video</h1>
            </div>
            <div class="col-lg-3 text-center">
                <h1>Database</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>About this assignment</h2>
                <p class="lead">
                    <strike>This page is all about demonstrating our skills in SQL by querying a given database and returning the
                    proper answers and values from it. This means that we are trying to execute the queries given in
                        our assignment and show that they are the same and work together!</strike>
                </p>
            </div>
        </div>
    </div>
</section>


<?php

include_once "obj/footer.php";

?>