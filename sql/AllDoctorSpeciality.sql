
CREATE VIEW AllDocSpecView AS
SELECT
   FirstName,
   LastName,
   SpecialityName 
FROM
   (
      SELECT
         Person.PersonId,
         Person.FirstName,
         Person.LastName 
      FROM
         Person,
         Doctor 
      WHERE
         Person.PersonId = Doctor.PersonID
   )
   AS t1 
   LEFT JOIN
      (
         SELECT DISTINCT
            Person.PersonId,
            Speciality.SpecialityName 
         FROM
            Person,
            Doctor,
            DoctorSpeciality,
            Speciality 
         WHERE
            Person.PersonId = Doctor.PersonID 
            AND Doctor.DoctorID = DoctorSpeciality.DoctorID 
            AND DoctorSpeciality.SpecialityID = Speciality.SpecialityID
      )
      AS t2 
      ON t1.PersonId = t2.PersonId

