-- --------------------------------------------------------
-- Host:                         csuf-mds-db.c6d5f3fxm8ox.us-west-1.rds.amazonaws.com
-- Server version:               10.2.21-MariaDB-log - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for DocOffice
DROP DATABASE IF EXISTS `DocOffice2`;
CREATE DATABASE IF NOT EXISTS `DocOffice2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `DocOffice2`;

-- Dumping structure for table DocOffice.Audit
DROP TABLE IF EXISTS `Audit`;
CREATE TABLE IF NOT EXISTS `Audit` (
  `FirstName` varchar(50) DEFAULT NULL,
  `ActionType` varchar(50) DEFAULT NULL,
  `Speciality` varchar(50) DEFAULT NULL,
  `ModDate` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.Audit: ~9 rows (approximately)
/*!40000 ALTER TABLE `Audit` DISABLE KEYS */;
INSERT INTO `Audit` (`FirstName`, `ActionType`, `Speciality`, `ModDate`) VALUES
	('Robert', 'UPDATE', '1', '2020-05-05 00:18:27'),
	('Anh', 'INSERT', '1', '2020-05-05 00:18:38'),
	('DJang', 'INSERT', '1', '2020-05-07 02:53:43'),
	('DJang', 'UPDATE', '3', '2020-05-07 02:53:47'),
	('DJang', 'INSERT', '1', '2020-05-07 05:24:14'),
	('Robert', 'UPDATE', '2', '2020-05-07 05:30:49'),
	('DJang', 'UPDATE', '2', '2020-05-07 05:38:18'),
	('Robert', 'INSERT', '1', '2020-05-07 05:41:22'),
	('Robert', 'UPDATE', '1', '2020-05-07 05:48:25');
/*!40000 ALTER TABLE `Audit` ENABLE KEYS */;

-- Dumping structure for table DocOffice.Doctor
DROP TABLE IF EXISTS `Doctor`;
CREATE TABLE IF NOT EXISTS `Doctor` (
  `DoctorID` int(11) NOT NULL AUTO_INCREMENT,
  `MedicalDegrees` varchar(50) DEFAULT NULL,
  `PersonID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DoctorID`),
  UNIQUE KEY `PersonID` (`PersonID`),
  CONSTRAINT `FK_Doctor_Person` FOREIGN KEY (`PersonID`) REFERENCES `Person` (`PersonId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.Doctor: ~3 rows (approximately)
/*!40000 ALTER TABLE `Doctor` DISABLE KEYS */;
INSERT INTO `Doctor` (`DoctorID`, `MedicalDegrees`, `PersonID`) VALUES
	(1, 'Biology', 2),
	(2, 'Liberal Arts', 7),
	(3, 'Pediatric', 10);
/*!40000 ALTER TABLE `Doctor` ENABLE KEYS */;

-- Dumping structure for table DocOffice.DoctorSpeciality
DROP TABLE IF EXISTS `DoctorSpeciality`;
CREATE TABLE IF NOT EXISTS `DoctorSpeciality` (
  `DoctorID` int(11) DEFAULT NULL,
  `SpecialityID` int(11) DEFAULT NULL,
  UNIQUE KEY `DoctorID_SpecialityID` (`DoctorID`,`SpecialityID`),
  KEY `FK_DoctorSpeciality_Speciality` (`SpecialityID`),
  CONSTRAINT `FK_DoctorSpeciality_Doctor` FOREIGN KEY (`DoctorID`) REFERENCES `Doctor` (`DoctorID`),
  CONSTRAINT `FK_DoctorSpeciality_Speciality` FOREIGN KEY (`SpecialityID`) REFERENCES `Speciality` (`SpecialityID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.DoctorSpeciality: ~5 rows (approximately)
/*!40000 ALTER TABLE `DoctorSpeciality` DISABLE KEYS */;
INSERT INTO `DoctorSpeciality` (`DoctorID`, `SpecialityID`) VALUES
	(1, 1),
	(2, 1),
	(2, 2),
	(2, 3),
	(3, 2);
/*!40000 ALTER TABLE `DoctorSpeciality` ENABLE KEYS */;

-- Dumping structure for table DocOffice.Patient
DROP TABLE IF EXISTS `Patient`;
CREATE TABLE IF NOT EXISTS `Patient` (
  `PatientID` int(11) NOT NULL AUTO_INCREMENT,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `PersonID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PatientID`),
  KEY `FK_Patient_Person` (`PersonID`),
  CONSTRAINT `FK_Patient_Person` FOREIGN KEY (`PersonID`) REFERENCES `Person` (`PersonId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.Patient: ~3 rows (approximately)
/*!40000 ALTER TABLE `Patient` DISABLE KEYS */;
INSERT INTO `Patient` (`PatientID`, `PhoneNumber`, `DOB`, `PersonID`) VALUES
	(1, '7147588854', '1959-05-08', 1),
	(2, '9685453512', '1997-01-11', 4),
	(3, '9856324580', '2000-06-28', 6);
/*!40000 ALTER TABLE `Patient` ENABLE KEYS */;

-- Dumping structure for table DocOffice.PatientVisit
DROP TABLE IF EXISTS `PatientVisit`;
CREATE TABLE IF NOT EXISTS `PatientVisit` (
  `VisitID` int(11) NOT NULL AUTO_INCREMENT,
  `PatientID` int(11) DEFAULT NULL,
  `DoctorID` int(11) DEFAULT NULL,
  `VisitDate` date DEFAULT NULL,
  `DocNote` text DEFAULT NULL,
  PRIMARY KEY (`VisitID`),
  KEY `FK_PatientVisit_Patient` (`PatientID`),
  KEY `FK_PatientVisit_Doctor` (`DoctorID`),
  CONSTRAINT `FK_PatientVisit_Doctor` FOREIGN KEY (`DoctorID`) REFERENCES `Doctor` (`DoctorID`),
  CONSTRAINT `FK_PatientVisit_Patient` FOREIGN KEY (`PatientID`) REFERENCES `Patient` (`PatientID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.PatientVisit: ~5 rows (approximately)
/*!40000 ALTER TABLE `PatientVisit` DISABLE KEYS */;
INSERT INTO `PatientVisit` (`VisitID`, `PatientID`, `DoctorID`, `VisitDate`, `DocNote`) VALUES
	(1, 2, 1, '2020-04-28', 'Patient was healthy, lots of rest'),
	(3, 2, 1, '2020-04-27', 'Follow up next day'),
	(4, 1, 2, '2020-01-26', 'No changes'),
	(5, 3, 2, '2015-04-28', 'Medication given'),
	(6, 3, 1, '2020-05-06', 'All good');
/*!40000 ALTER TABLE `PatientVisit` ENABLE KEYS */;

-- Dumping structure for table DocOffice.Person
DROP TABLE IF EXISTS `Person`;
CREATE TABLE IF NOT EXISTS `Person` (
  `PersonId` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `StreetAddress` varchar(150) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `State` varchar(50) DEFAULT NULL,
  `Zip` varchar(50) DEFAULT NULL,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `SSN` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`PersonId`),
  UNIQUE KEY `SSN` (`SSN`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.Person: ~7 rows (approximately)
/*!40000 ALTER TABLE `Person` DISABLE KEYS */;
INSERT INTO `Person` (`PersonId`, `FirstName`, `LastName`, `StreetAddress`, `City`, `State`, `Zip`, `PhoneNumber`, `SSN`) VALUES
	(1, 'Alex', 'Sigler', '2060 Madison', 'La Mirada', 'CA', '90638', '7147685534', '123456789'),
	(2, 'Robert', 'Stevens', '3 Street Street', 'Fullerton', 'CA', '92456', '1234567890', '321456987'),
	(4, 'Bob', 'Sigler', '2060 Madison', 'La Mirada', 'CA', '90638', '7147625534', '123456779'),
	(5, 'George', 'Smith', '12345 Beach Blvd', 'Costa Mesa', 'CA', '78945', '1456354890', '123546951'),
	(6, 'Smithy', 'Smitherson', '987 WhiteHouse', 'New York', 'George', '96421', '6549873512', '987520314'),
	(7, 'Anh', 'Nguyen', '9520 Spooner Street', 'Orlando', 'Florida', '74582', '6543219805', '968574231'),
	(10, 'DJang', 'Change', '12395 Heaven Road', 'Santa Ana', 'Ohio', '98725', '714659943', '154265409');
/*!40000 ALTER TABLE `Person` ENABLE KEYS */;

-- Dumping structure for table DocOffice.Prescription
DROP TABLE IF EXISTS `Prescription`;
CREATE TABLE IF NOT EXISTS `Prescription` (
  `PrescriptionID` int(11) NOT NULL AUTO_INCREMENT,
  `PrescriptionName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PrescriptionID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.Prescription: ~4 rows (approximately)
/*!40000 ALTER TABLE `Prescription` DISABLE KEYS */;
INSERT INTO `Prescription` (`PrescriptionID`, `PrescriptionName`) VALUES
	(1, 'Vicodin'),
	(2, 'Tylenol'),
	(3, 'Morphine'),
	(4, 'Advil');
/*!40000 ALTER TABLE `Prescription` ENABLE KEYS */;

-- Dumping structure for table DocOffice.PVisitPrescription
DROP TABLE IF EXISTS `PVisitPrescription`;
CREATE TABLE IF NOT EXISTS `PVisitPrescription` (
  `VisitID` int(11) DEFAULT NULL,
  `PrescriptionID` int(11) DEFAULT NULL,
  KEY `FK_PVisitPrescription_PatientVisit` (`VisitID`),
  KEY `FK_PVisitPrescription_Prescription` (`PrescriptionID`),
  CONSTRAINT `FK_PVisitPrescription_PatientVisit` FOREIGN KEY (`VisitID`) REFERENCES `PatientVisit` (`VisitID`),
  CONSTRAINT `FK_PVisitPrescription_Prescription` FOREIGN KEY (`PrescriptionID`) REFERENCES `Prescription` (`PrescriptionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.PVisitPrescription: ~6 rows (approximately)
/*!40000 ALTER TABLE `PVisitPrescription` DISABLE KEYS */;
INSERT INTO `PVisitPrescription` (`VisitID`, `PrescriptionID`) VALUES
	(3, 3),
	(1, 1),
	(1, 3),
	(3, 1),
	(5, 4),
	(5, 1);
/*!40000 ALTER TABLE `PVisitPrescription` ENABLE KEYS */;

-- Dumping structure for table DocOffice.PVisitTest
DROP TABLE IF EXISTS `PVisitTest`;
CREATE TABLE IF NOT EXISTS `PVisitTest` (
  `VisitID` int(11) DEFAULT NULL,
  `TestID` int(11) DEFAULT NULL,
  KEY `FK_PVisitTest_PatientVisit` (`VisitID`),
  KEY `FK_PVisitTest_Test` (`TestID`),
  CONSTRAINT `FK_PVisitTest_PatientVisit` FOREIGN KEY (`VisitID`) REFERENCES `PatientVisit` (`VisitID`),
  CONSTRAINT `FK_PVisitTest_Test` FOREIGN KEY (`TestID`) REFERENCES `Test` (`TestID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.PVisitTest: ~3 rows (approximately)
/*!40000 ALTER TABLE `PVisitTest` DISABLE KEYS */;
INSERT INTO `PVisitTest` (`VisitID`, `TestID`) VALUES
	(3, 1),
	(1, 2),
	(5, 2);
/*!40000 ALTER TABLE `PVisitTest` ENABLE KEYS */;

-- Dumping structure for table DocOffice.Speciality
DROP TABLE IF EXISTS `Speciality`;
CREATE TABLE IF NOT EXISTS `Speciality` (
  `SpecialityID` int(11) NOT NULL AUTO_INCREMENT,
  `SpecialityName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`SpecialityID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.Speciality: ~3 rows (approximately)
/*!40000 ALTER TABLE `Speciality` DISABLE KEYS */;
INSERT INTO `Speciality` (`SpecialityID`, `SpecialityName`) VALUES
	(1, 'Dentistry'),
	(2, 'Vision'),
	(3, 'Pediatrician');
/*!40000 ALTER TABLE `Speciality` ENABLE KEYS */;

-- Dumping structure for table DocOffice.Test
DROP TABLE IF EXISTS `Test`;
CREATE TABLE IF NOT EXISTS `Test` (
  `TestID` int(11) NOT NULL AUTO_INCREMENT,
  `TestName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`TestID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table DocOffice.Test: ~4 rows (approximately)
/*!40000 ALTER TABLE `Test` DISABLE KEYS */;
INSERT INTO `Test` (`TestID`, `TestName`) VALUES
	(1, 'COVID-19'),
	(2, 'HIV'),
	(3, 'Bloodwork'),
	(4, 'Flu');
/*!40000 ALTER TABLE `Test` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
