CREATE VIEW Vicodin AS 
SELECT
   Person.FirstName,
   Person.LastName 
FROM
   Person 
WHERE
   Person.PersonId IN 
   (
      SELECT
         Doctor.PersonID 
      FROM
         Doctor 
      WHERE
         Doctor.DoctorID IN 
         (
            SELECT DISTINCT
               PatientVisit.DoctorID 
            FROM
               PatientVisit 
            WHERE
               PatientVisit.VisitID IN 
               (
                  SELECT
                     PVisitPrescription.VisitID 
                  FROM
                     PVisitPrescription 
                  WHERE
                     PVisitPrescription.PrescriptionID = 
                     (
                        SELECT
                           Prescription.PrescriptionID 
                        FROM
                           Prescription 
                        WHERE
                           Prescription.PrescriptionName = "Vicodin"
                     )
               )
         )
   )
;

