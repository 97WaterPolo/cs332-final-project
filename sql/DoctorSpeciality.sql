CREATE VIEW DocSpecView AS
SELECT
  DISTINCT Person.FirstName,
  Person.LastName,
  Speciality.SpecialityName
FROM
  Person,
  Doctor,
  DoctorSpeciality,
  Speciality
WHERE
  Person.PersonId = Doctor.PersonID
  AND Doctor.DoctorID = DoctorSpeciality.DoctorID
  AND DoctorSpeciality.SpecialityID = Speciality.SpecialityID;
