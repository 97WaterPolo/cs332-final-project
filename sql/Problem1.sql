SELECT
   Person.FirstName,
   Person.LastName,
   Patient.PhoneNumber 
FROM
   Person,
   Patient 
WHERE
   Person.PersonId = Patient.PersonID 
   AND Person.PersonId IN 
   (
      SELECT
         Patient.PersonID 
      FROM
         Patient 
      WHERE
         Patient.PatientID IN 
         (
            SELECT DISTINCT
               PatientVisit.PatientID 
            FROM
               PatientVisit 
            WHERE
               PatientVisit.DoctorID = 
               (
                  SELECT
                     Doctor.DoctorID 
                  FROM
                     Doctor 
                  WHERE
                     Doctor.PersonID = 
                     (
                        SELECT
                           Person.PersonId 
                        FROM
                           Person 
                        WHERE
                           Person.LastName = "Stevens" 
                     )
               )
         )
   )

