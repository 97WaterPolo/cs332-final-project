DROP TRIGGER IF EXISTS docAuditInsert;
DROP TRIGGER IF EXISTS docAuditUpdate;
CREATE TRIGGER docAuditInsert AFTER INSERT 
ON DoctorSpeciality FOR EACH ROW 
INSERT INTO
   Audit(`FirstName`, `ActionType`, `Speciality`) 
VALUES
   (
(
      SELECT
         Person.FirstName 
      FROM
         Person 
      WHERE
         Person.PersonId = 
         (
            SELECT
               Doctor.PersonID 
            FROM
               Doctor 
            WHERE
               Doctor.DoctorID = NEW.DoctorID
         )
),
         "INSERT",
         NEW.SpecialityID
   )
;
CREATE TRIGGER docAuditUpdate AFTER 
UPDATE
   ON DoctorSpeciality FOR EACH ROW 
   INSERT INTO
      Audit(`FirstName`, `ActionType`, `Speciality`) 
   VALUES
      (
(
         SELECT
            Person.FirstName 
         FROM
            Person 
         WHERE
            Person.PersonId = 
            (
               SELECT
                  Doctor.PersonID 
               FROM
                  Doctor 
               WHERE
                  Doctor.DoctorID = NEW.DoctorID
            )
),
            "UPDATE",
               NEW.SpecialityID
      )
;

