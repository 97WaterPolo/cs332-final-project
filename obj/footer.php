<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/17/2020
 * Time: 1:43 PM
 */
?>


<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Alexander Sigler 2020</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/../vendor/jquery/jquery.min.js"></script>
<script src="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/../vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom JavaScript for this theme -->
<script src="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/../js/scrolling-nav.js"></script>

</body>

</html>
