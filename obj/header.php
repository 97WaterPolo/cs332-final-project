<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/17/2020
 * Time: 1:43 PM
 */

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alexander Sigler - CS323</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/../css/scrolling-nav.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php" <?php echo $activeQuery == 0 ? "active" : "" ?>>Alexander Sigler's Final Project</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="index.php">
                        <i class="fa fa-home"></i>
                        Home</a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link js-scroll-trigger" href="directory.php">
                        <i class="fa fa-pencil-square"></i>
                        Problems
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" target="_blank" href="https://bitbucket.org/97WaterPolo/">
                        <i class="fa fa-bitbucket"></i>
                        BitBucket
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link js-scroll-trigger" target="_blank" href="https://github.com/97WaterPolo/">
                        <i class="fa fa-github"></i>
                        GitHub
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link js-scroll-trigger" target="_blank" href="https://siglerdev.us">
                        <i class="fa fa-internet-explorer"></i>
                        Website
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>