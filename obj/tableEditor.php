<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 5/6/2020
 * Time: 7:45 PM
 */

if (isset($changeErrorMsg)){
    echo '    <div class="alert alert-primary text-center" role="alert">
    '.$changeErrorMsg.'
    </div>';
}
?>

<form autocomplete="off" method="post" action="">
    <div id="create">
        <select name="doctorId">
            <option disabled selected>--Select Doctor ID--</option>
            <?php
            $stmt= $link->prepare("SELECT Person.LastName, Doctor.DoctorID FROM Person,Doctor WHERE Person.PersonId = Doctor.PersonID;");
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            foreach ($result as $r){
                echo '<option value="'.$r['DoctorID'].'">Dr. '.$r['LastName'].'</option>';
            }
            ?>
        </select>
        <select name="specialityId">
            <option disabled selected>--Select Speciality ID--</option>
            <?php
            $stmt= $link->prepare("SELECT * FROM Speciality");
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            foreach ($result as $r){
                echo '<option value="'.$r['SpecialityID'].'">'.$r['SpecialityName'].'</option>';
            }
            ?>
        </select>
        <button class="btn btn-success" type="submit"  name="createEntry">Create</button>
    </div>
    <div id="update">
        <select name="doctorId">
            <option disabled selected>--Select Doctor ID--</option>
            <?php
            $stmt= $link->prepare("SELECT Person.LastName, Doctor.DoctorID FROM Person,Doctor WHERE Person.PersonId = Doctor.PersonID;");
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            foreach ($result as $r){
                echo '<option value="'.$r['DoctorID'].'">Dr. '.$r['LastName'].'</option>';
            }
            ?>
        </select>
        <select name="specialityId">
            <option disabled selected>--Select Speciality ID--</option>
            <?php
            $stmt= $link->prepare("SELECT * FROM Speciality");
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            foreach ($result as $r){
                echo '<option value="'.$r['SpecialityID'].'">'.$r['SpecialityName'].'</option>';
            }
            ?>
        </select>
        <select name="newSpecialityId">
            <option selected>--Select New Speciality ID--</option>
            <?php
            $stmt= $link->prepare("SELECT * FROM Speciality");
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            foreach ($result as $r){
                echo '<option value="'.$r['SpecialityID'].'">'.$r['SpecialityName'].'</option>';
            }
            ?>
        </select>
        <button class="btn btn-primary" type="submit" name="updateEntry">Update</button>
    </div>
    <div id="delete">
        <select name="doctorId">
            <option disabled selected>--Select Doctor ID--</option>
            <?php
            $stmt= $link->prepare("SELECT Person.LastName, Doctor.DoctorID FROM Person,Doctor WHERE Person.PersonId = Doctor.PersonID;");
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            foreach ($result as $r){
                echo '<option value="'.$r['DoctorID'].'">Dr. '.$r['LastName'].'</option>';
            }
            ?>
        </select>
        <select name="specialityId">
            <option disabled selected>--Select Speciality ID--</option>
            <?php
            $stmt= $link->prepare("SELECT * FROM Speciality");
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            foreach ($result as $r){
                echo '<option value="'.$r['SpecialityID'].'">'.$r['SpecialityName'].'</option>';
            }
            ?>
        </select>
        <button class="btn btn-danger" type="submit" name="deleteEntry">Delete</button>
    </div>
</form>