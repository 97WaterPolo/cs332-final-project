
<?php
$activeQuery = "1a";
include_once "obj/header.php";


include_once "obj/connect.php";


$sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA='DocOffice' AND TABLE_NAME NOT LIKE 'BK_%'";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
?>

<section>
    <div class="alert alert-primary text-center" role="alert">
        <?php echo $sql; ?>
    </div>
    <div class="container">
        <p class="text-center" style="width: 50%; margin: 0 auto">
           This returns all the information in the database!
            <br><br>
        </p>
    </div>
    <div class="container text-center" style="justify-content: center">
        <?php
        foreach ($result as $value){
            $sql = "SELECT * FROM ".$value['TABLE_NAME'];
            $stmt= $link->prepare($sql);
            $stmt->execute();
            $tableValues = $stmt->get_result();
            $tableValues = $tableValues->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            print "<pre>";
            print "<h5>".$value['TABLE_NAME']."</h5>";
            print "<table border=1 style='margin: 0 auto'>";
            if (sizeof($tableValues) > 0) {
                print "<tr>";
                foreach (array_keys($tableValues[0]) as $key)
                    print "<td>$key</td>";
                print "</tr>";
                foreach ($tableValues as $r) {
                    print "<tr>";
                    foreach ($r as $value) {
                        print "<td>$value</td>";
                    }
                    print "</tr>";

                }
            }else
                print "No data found with that query. (No results found)";
            print "</table>";
            print "</pre>";
            echo '<br>';
        }



        ?>
    </div>
</section>

