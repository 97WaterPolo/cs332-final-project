
<?php
$activeQuery = "1b";
include_once "obj/header.php";


include_once "obj/connect.php";

$viewSql = " CREATE VIEW AllDocSpecView AS SELECT FirstName, LastName, SpecialityName FROM( SELECT Person.PersonId, Person.FirstName, Person.LastName FROM Person, Doctor WHERE Person.PersonId = Doctor.PersonID) AS t1 LEFT JOIN ( SELECT DISTINCT Person.PersonId, Speciality.SpecialityName FROM Person, Doctor, DoctorSpeciality, Speciality WHERE Person.PersonId = Doctor.PersonID AND Doctor.DoctorID = DoctorSpeciality.DoctorID AND DoctorSpeciality.SpecialityID = Speciality.SpecialityID ) AS t2 ON t1.PersonId = t2.PersonId ";
$sql = "SELECT * FROM AllDocSpecView;";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
?>

<section>
    <div class="alert alert-primary text-center" role="alert">
        <?php echo $viewSql; ?>
        <br><br>
        <b>Above statement creates the view, the second one gets the values from the view:</b><br>
        <?php echo $sql; ?>
    </div>
    <div class="container">
        <p class="text-center" style="width: 50%; margin: 0 auto">
            Gives a list of ALL the doctors with their specialities, and if they don't have one still include them.
            <br><br>
        </p>
    </div>
    <div class="container text-center" style="justify-content: center">
        <?php
        print "<pre>";
        print "<table border=1 style='margin: 0 auto'>";

        if (sizeof($result) > 0) {
            print "<tr>";
            foreach (array_keys($result[0]) as $key)
                print "<td>$key</td>";
            print "</tr>";
            foreach ($result as $r) {
                print "<tr>";
                foreach ($r as $value) {
                    print "<td>$value</td>";
                }
                print "</tr>";

            }
        }else
            print "No data found with that query. (No results found)";
        print "</table>";
        print "</pre>";
        echo '<br><br><br><br>';
        ?>
    </div>
</section>

