
<?php
$activeQuery = "1b";
include_once "obj/header.php";
include_once "obj/connect.php";


if (isset($_POST['createEntry'])){
    $stmt= $link->prepare("INSERT INTO DoctorSpeciality(DoctorId,SpecialityID) VALUES (?, ?)");
    $stmt->bind_param("ii", $_POST['doctorId'], $_POST['specialityId']);
    if ($stmt->execute()){
        $changeErrorMsg = "It was successfully created";
    }else
        $changeErrorMsg = ("Wasn't able to create the entry! Probably due to key/pair already existing!");

}else if (isset($_POST['updateEntry'])){
    $stmt= $link->prepare("UPDATE DoctorSpeciality SET DoctorSpeciality.SpecialityID=? WHERE  DoctorSpeciality.DoctorID=? AND DoctorSpeciality.SpecialityID=?");
    $stmt->bind_param("iii", $_POST['newSpecialityId'], $_POST['doctorId'], $_POST['specialityId']);
    if ($stmt->execute()){
        $changeErrorMsg = ("It was successfully updated");
    }else
        $changeErrorMsg = ("Wasn't able to update that entry!");
}else if (isset($_POST['deleteEntry'])){
    $stmt= $link->prepare("DELETE FROM DoctorSpeciality WHERE DoctorSpeciality.DoctorID=? AND DoctorSpeciality.SpecialityID=?");
    $stmt->bind_param("ii", $_POST['doctorId'], $_POST['specialityId']);
    if ($stmt->execute()){
        $changeErrorMsg = ("It was successfully deleted");
    }else
        $changeErrorMsg = ("Wasn't able to delete that entry!");

}


$viewSql = "DROP TRIGGER IF EXISTS docAuditInsert; DROP TRIGGER IF EXISTS docAuditUpdate; CREATE TRIGGER docAuditInsert AFTER INSERT ON DoctorSpeciality FOR EACH ROW INSERT INTO Audit(`FirstName`, `ActionType`, `Speciality`) VALUES( ( SELECT Person.FirstName FROM Person WHERE Person.PersonId = ( SELECT Doctor.PersonID FROM Doctor WHERE Doctor.DoctorID = NEW.DoctorID) ), \"INSERT\", NEW.SpecialityID ) ; CREATE TRIGGER docAuditUpdate AFTER UPDATE ON DoctorSpeciality FOR EACH ROW INSERT INTO Audit(`FirstName`, `ActionType`, `Speciality`) VALUES ( ( SELECT Person.FirstName FROM Person WHERE Person.PersonId = ( SELECT Doctor.PersonID FROM Doctor WHERE Doctor.DoctorID = NEW.DoctorID ) ), \"UPDATE\", NEW.SpecialityID ) ; ";
$sqlAud = "SELECT * FROM Audit";
$stmt= $link->prepare($sqlAud);
$stmt->execute();
$auditResult = $stmt->get_result();
$auditResult = $auditResult->fetch_all(MYSQLI_ASSOC); // fetch an array of rows

$sql = "SELECT * FROM DoctorSpeciality";
$stmt= $link->prepare($sql);
$stmt->execute();
$docResult = $stmt->get_result();
$docResult = $docResult->fetch_all(MYSQLI_ASSOC); // fetch an array of rows

$sql = "SELECT * FROM Speciality";
$stmt= $link->prepare($sql);
$stmt->execute();
$spec = $stmt->get_result();
$spec = $spec->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
?>

<div>
    <div class="alert alert-primary text-center" role="alert">
        <?php echo str_replace(";", "<br><br>", $viewSql); ?>
        <br><br>
        <b>Above statement creates the triggers, the second one gets the values from the audit table:</b><br>
        <?php echo $sqlAud; ?>
    </div>
    <div class="container">
        <p class="text-center" style="width: 50%; margin: 0 auto">
            Proof that the Audit table works and can track changes to values!
            <br><br>
        </p>
    </div>
    <div class="text-center">
        <h2>Modify the tables VIA SQL</h2>
        <?php include_once 'obj/tableEditor.php';?>
        <br><br>
    </div>
    <div class="container text-center" style="justify-content: center">
        <?php
        print "<pre>";
        print "<h2>Audit</h2>";
        print "<table border=1 style='margin: 0 auto'>";

        if (sizeof($auditResult) > 0) {
            print "<tr>";
            foreach (array_keys($auditResult[0]) as $key)
                print "<td>$key</td>";
            print "</tr>";
            foreach ($auditResult as $r) {
                print "<tr>";
                foreach ($r as $value) {
                    print "<td>$value</td>";
                }
                print "</tr>";

            }
        }else
            print "No data found with that query. (No results found)";
        print "</table>";
        print "</pre>";
        echo '<br>';
        ?>
        <?php
        print "<pre>";
        print "<h2>DoctorSpeciality</h2>";
        print "<table border=1 style='margin: 0 auto'>";

        if (sizeof($docResult) > 0) {
            print "<tr>";
            foreach (array_keys($docResult[0]) as $key)
                print "<td>$key</td>";
            print "</tr>";
            foreach ($docResult as $r) {
                print "<tr>";
                foreach ($r as $value) {
                    print "<td>$value</td>";
                }
                print "</tr>";

            }
        }else
            print "No data found with that query. (No results found)";
        print "</table>";
        print "</pre>";
        echo '<br>';
        ?>
        <?php
        print "<pre>";
        print "<h2>Speciality</h2>";
        print "<table border=1 style='margin: 0 auto'>";

        if (sizeof($spec) > 0) {
            print "<tr>";
            foreach (array_keys($spec[0]) as $key)
                print "<td>$key</td>";
            print "</tr>";
            foreach ($spec as $r) {
                print "<tr>";
                foreach ($r as $value) {
                    print "<td>$value</td>";
                }
                print "</tr>";

            }
        }else
            print "No data found with that query. (No results found)";
        print "</table>";
        print "</pre>";
        echo '<br>';
        ?>
    </div>

</div>

