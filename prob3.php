
<?php
$activeQuery = "1b";
include_once "obj/header.php";


include_once "obj/connect.php";

$viewSql = "CREATE VIEW Vicodin AS SELECT Person.FirstName, Person.LastName FROM Person WHERE Person.PersonId IN( SELECT Doctor.PersonID FROM Doctor WHERE Doctor.DoctorID IN ( SELECT DISTINCT PatientVisit.DoctorID FROM PatientVisit WHERE PatientVisit.VisitID IN ( SELECT PVisitPrescription.VisitID FROM PVisitPrescription WHERE PVisitPrescription.PrescriptionID = ( SELECT Prescription.PrescriptionID FROM Prescription WHERE Prescription.PrescriptionName = \"Vicodin\") ) ) ) ; ";
$sql = "SELECT * FROM Vicodin;";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
?>

<section>
    <div class="alert alert-primary text-center" role="alert">
        <?php echo $viewSql; ?>
        <br><br>
        <b>Above statement creates the view, the second one gets the values from the view:</b><br>
        <?php echo $sql; ?>
    </div>
    <div class="container">
        <p class="text-center" style="width: 50%; margin: 0 auto">
            This query gets a list of all the Doctors that have prescribed Vicodin. This comes from a view that is in the database
            <br><br>
        </p>
    </div>
    <div class="container text-center" style="justify-content: center">
        <?php
        print "<pre>";
        print "<table border=1 style='margin: 0 auto'>";

        if (sizeof($result) > 0) {
            print "<tr>";
            foreach (array_keys($result[0]) as $key)
                print "<td>$key</td>";
            print "</tr>";
            foreach ($result as $r) {
                print "<tr>";
                foreach ($r as $value) {
                    print "<td>$value</td>";
                }
                print "</tr>";

            }
        }else
            print "No data found with that query. (No results found)";
        print "</table>";
        print "</pre>";
        echo '<br><br><br><br>';
        ?>
    </div>
</section>

